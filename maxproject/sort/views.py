from django.shortcuts import render
from .forms import SortForm
from .sorts import Sorts


def index(request):
    if request.method == 'POST':
        filled_form = SortForm(request.POST, request.FILES)
        if filled_form.is_valid():
            data = request.FILES['file'].read()
            data = data.decode("utf-8")
            list_data = data.split()

            new_form, note = None, None
            for i in range(len(list_data)):
                list_data[i] = int(list_data[i])
            if filled_form.cleaned_data['sorts'] == 'Bubble':
                note = 'It is a bubblesort! The original integers are %s' % data
                Sorts.bubble_sort(list_data)
                new_form = list_data
            elif filled_form.cleaned_data['sorts'] == 'Merge':
                note = 'It is a mergesort! The original integers are %s' % data
                Sorts.merge_sort(list_data)
                new_form = list_data
            elif filled_form.cleaned_data['sorts'] == 'Insertion':
                note = 'It is a insertion sort! The original integers are %s' % data
                Sorts.insertion_sort(list_data)
                new_form = list_data
            return render(request, 'sort/index.html', {'sortform': filled_form, 'note': note, 'form': new_form})
    else:
        form = SortForm()
        note = "Lets do this"
        return render(request, 'sort/index.html', {'sortform': form, 'note': note})
