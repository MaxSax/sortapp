from django import forms


class SortForm(forms.Form):
    sorts = forms.ChoiceField(
        label='Sorts',
        choices=[
            ('Bubble', 'Bubble'),
            ('Insertion', 'Insertion'),
            ('Merge', 'Merge')
        ]
    )
    file = forms.FileField()
