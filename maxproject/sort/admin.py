from django.contrib import admin
from .models import SortAlgorithm

admin.site.register(SortAlgorithm)
