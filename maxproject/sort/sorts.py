import time


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()

        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%r  %2.2f ms' %
                  (method.__name__, (te - ts) * 1000))
        return result
    return timed


class Sorts:

    @staticmethod
    @timeit
    def bubble_sort(arr):
        for i in range(len(arr)-1):
            for j in range(0, len(arr)-i-1):
                if arr[j] > arr[j+1]:
                    arr[j], arr[j+1] = arr[j+1], arr[j]

    @staticmethod
    @timeit
    def insertion_sort(arr):
        for i in range(1, len(arr)):
            key = arr[i]
            j = i-1
            while j >= 0 and key < arr[j]:
                arr[j+1] = arr[j]
                j -= 1
            arr[j+1] = key

    @staticmethod
    @timeit
    def merge_sort(arr):
        if len(arr) > 1:
            mid = len(arr)//2
            left_part = arr[:mid]
            right_part = arr[mid:]

            Sorts.merge_sort(left_part)
            Sorts.merge_sort(right_part)

            i = j = k = 0
            while i < len(left_part) and j < len(right_part):
                if left_part[i] < right_part[j]:
                    arr[k] = left_part[i]
                    i += 1
                else:
                    arr[k] = right_part[j]
                    j += 1
                k += 1

            while i < len(left_part):
                arr[k] = left_part[i]
                i += 1
                k += 1

            while j < len(right_part):
                arr[k] = right_part[j]
                j += 1
                k += 1
